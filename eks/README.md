## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14.8 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 3.41.0 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | 2.2.0 |
| <a name="requirement_local"></a> [local](#requirement\_local) | 2.1.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | 3.1.0 |
| <a name="requirement_tls"></a> [tls](#requirement\_tls) | 3.1.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.41.0 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.1.0 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.1.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_eks"></a> [eks](#module\_eks) | git::https://github.com/terraform-aws-modules/terraform-aws-eks.git | v16.1.0 |
| <a name="module_vpc"></a> [vpc](#module\_vpc) | git::https://github.com/terraform-aws-modules/terraform-aws-vpc.git | v3.0.0 |

## Resources

| Name | Type |
|------|------|
| [aws_s3_bucket_object.kubeconf_file](https://registry.terraform.io/providers/hashicorp/aws/3.41.0/docs/resources/s3_bucket_object) | resource |
| [aws_security_group.all_worker_mgmt](https://registry.terraform.io/providers/hashicorp/aws/3.41.0/docs/resources/security_group) | resource |
| [aws_security_group.worker_group_mgmt_one](https://registry.terraform.io/providers/hashicorp/aws/3.41.0/docs/resources/security_group) | resource |
| [aws_security_group.worker_group_mgmt_two](https://registry.terraform.io/providers/hashicorp/aws/3.41.0/docs/resources/security_group) | resource |
| [null_resource.flux-bootstrap](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.flux-git-source](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.flux-image-automation-update](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.flux-image-policy](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.flux-image-scanning](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [random_string.suffix](https://registry.terraform.io/providers/hashicorp/random/3.1.0/docs/resources/string) | resource |
| [aws_availability_zones.available](https://registry.terraform.io/providers/hashicorp/aws/3.41.0/docs/data-sources/availability_zones) | data source |
| [aws_eks_cluster.cluster](https://registry.terraform.io/providers/hashicorp/aws/3.41.0/docs/data-sources/eks_cluster) | data source |
| [aws_eks_cluster_auth.cluster](https://registry.terraform.io/providers/hashicorp/aws/3.41.0/docs/data-sources/eks_cluster_auth) | data source |
| [aws_s3_bucket.found_bucket](https://registry.terraform.io/providers/hashicorp/aws/3.41.0/docs/data-sources/s3_bucket) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_GITLAB_TOKEN"></a> [GITLAB\_TOKEN](#input\_GITLAB\_TOKEN) | n/a | `string` | n/a | yes |
| <a name="input_cluster_name"></a> [cluster\_name](#input\_cluster\_name) | n/a | `string` | `"docs-devops"` | no |
| <a name="input_cluster_name-"></a> [cluster\_name-](#input\_cluster\_name-) | n/a | `string` | `""` | no |
| <a name="input_flux_branch"></a> [flux\_branch](#input\_flux\_branch) | n/a | `string` | n/a | yes |
| <a name="input_flux_commit_mail"></a> [flux\_commit\_mail](#input\_flux\_commit\_mail) | n/a | `string` | n/a | yes |
| <a name="input_flux_commit_name"></a> [flux\_commit\_name](#input\_flux\_commit\_name) | n/a | `string` | n/a | yes |
| <a name="input_flux_extra_components"></a> [flux\_extra\_components](#input\_flux\_extra\_components) | n/a | `string` | n/a | yes |
| <a name="input_flux_gitsource_repo_alias"></a> [flux\_gitsource\_repo\_alias](#input\_flux\_gitsource\_repo\_alias) | n/a | `string` | n/a | yes |
| <a name="input_flux_gitsource_repo_url"></a> [flux\_gitsource\_repo\_url](#input\_flux\_gitsource\_repo\_url) | n/a | `string` | n/a | yes |
| <a name="input_flux_image"></a> [flux\_image](#input\_flux\_image) | n/a | `string` | n/a | yes |
| <a name="input_flux_image_alias"></a> [flux\_image\_alias](#input\_flux\_image\_alias) | n/a | `string` | n/a | yes |
| <a name="input_flux_image_repo"></a> [flux\_image\_repo](#input\_flux\_image\_repo) | n/a | `string` | n/a | yes |
| <a name="input_flux_owner"></a> [flux\_owner](#input\_flux\_owner) | n/a | `string` | n/a | yes |
| <a name="input_flux_repository"></a> [flux\_repository](#input\_flux\_repository) | n/a | `string` | n/a | yes |
| <a name="input_flux_semver"></a> [flux\_semver](#input\_flux\_semver) | n/a | `string` | n/a | yes |
| <a name="input_flux_target_path"></a> [flux\_target\_path](#input\_flux\_target\_path) | n/a | `string` | n/a | yes |
| <a name="input_map_accounts"></a> [map\_accounts](#input\_map\_accounts) | Additional AWS account numbers to add to the aws-auth configmap. | `list(string)` | <pre>[<br>  "871212273515"<br>]</pre> | no |
| <a name="input_map_roles"></a> [map\_roles](#input\_map\_roles) | Additional IAM roles to add to the aws-auth configmap. | <pre>list(object({<br>    rolearn  = string<br>    username = string<br>    groups   = list(string)<br>  }))</pre> | <pre>[<br>  {<br>    "groups": [<br>      "system:masters"<br>    ],<br>    "rolearn": "arn:aws:iam::871212273515:role/eks_role1",<br>    "username": "eks_role1"<br>  }<br>]</pre> | no |
| <a name="input_map_users"></a> [map\_users](#input\_map\_users) | Additional IAM users to add to the aws-auth configmap. | <pre>list(object({<br>    userarn  = string<br>    username = string<br>    groups   = list(string)<br>  }))</pre> | <pre>[<br>  {<br>    "groups": [<br>      "system:masters"<br>    ],<br>    "userarn": "arn:aws:iam::871212273515:user/mrbot",<br>    "username": "mrbot"<br>  }<br>]</pre> | no |
| <a name="input_region"></a> [region](#input\_region) | n/a | `string` | `"us-east-1"` | no |
| <a name="input_s3_bucket"></a> [s3\_bucket](#input\_s3\_bucket) | n/a | `string` | n/a | yes |

## Outputs

No outputs.

resource "null_resource" "flux-bootstrap" {
  triggers = {
    key = uuid()
  }
  provisioner "local-exec" {
     interpreter = ["/bin/bash" ,"-c"]
     command = <<-EOT
         flux bootstrap gitlab \
         --owner="${var.flux_owner}" \
         --repository="${var.flux_repository}" \
         --branch="${var.flux_branch}" \
         --path="${var.flux_target_path}" \
         --components-extra="${var.flux_extra_components}" \
         --token-auth
     EOT
    environment = {
      KUBECONFIG = module.eks.kubeconfig_filename
      GITLAB_TOKEN= var.GITLAB_TOKEN
    }
  }
}
resource "null_resource" "flux-image-scanning" {
  triggers = {
    key = uuid()
  }
  provisioner "local-exec" {
     interpreter = ["/bin/bash" ,"-c"]
     command = <<-EOT
         flux create image repository "${var.flux_image_repo}" \
         --image="${var.flux_image}" \
         --interval=1m
     EOT
    environment = {
      KUBECONFIG = module.eks.kubeconfig_filename
      GITLAB_TOKEN= var.GITLAB_TOKEN
    }
  }
  depends_on = [null_resource.flux-bootstrap]
}

resource "null_resource" "flux-image-policy" {
  triggers = {
    key = uuid()
  }
  provisioner "local-exec" {
     interpreter = ["/bin/bash" ,"-c"]
     command = <<-EOT
         flux create image policy "${var.flux_image_alias}" \
         --image-ref="${var.flux_image_alias}" \
         --select-semver="${var.flux_semver}"
     EOT
    environment = {
      KUBECONFIG = module.eks.kubeconfig_filename
      GITLAB_TOKEN= var.GITLAB_TOKEN
    }
  }
  depends_on = [null_resource.flux-bootstrap]
}

resource "null_resource" "flux-image-automation-update" {
  triggers = {
    key = uuid()
  }
  provisioner "local-exec" {
     interpreter = ["/bin/bash" ,"-c"]
     command = <<-EOT
         flux create image update "${var.flux_image_alias}" \
         --git-repo-ref="${var.flux_repository}" \
         --git-repo-path="${var.flux_target_path}" \
         --checkout-branch="${var.flux_branch}" \
         --push-branch="${var.flux_branch}" \
         --author-name="${var.flux_commit_name}" \
         --author-email="${var.flux_commit_mail}"\
         --commit-template="{{range .Updated.Images}}{{println .}}{{end}}"
     EOT
    environment = {
      KUBECONFIG = module.eks.kubeconfig_filename
      GITLAB_TOKEN= var.GITLAB_TOKEN
    }
  }
  depends_on = [null_resource.flux-bootstrap]
}

resource "null_resource" "flux-git-source" {
  triggers = {
    key = uuid()
  }
  provisioner "local-exec" {
     interpreter = ["/bin/bash" ,"-c"]
     command = <<-EOT
         flux create source git "${var.flux_gitsource_repo_alias}" \
         --url="${var.flux_gitsource_repo_url}" \
         --branch="${var.flux_branch}" \
         --interval=30s
     EOT
    environment = {
      KUBECONFIG = module.eks.kubeconfig_filename
      GITLAB_TOKEN= var.GITLAB_TOKEN
    }
  }
  depends_on = [null_resource.flux-bootstrap]
}
